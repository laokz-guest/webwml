#use wml::debian::template title="Debian &ldquo;jessie&rdquo; Installatie-informatie" NOHEADER="true"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/jessie/release.data"
#use wml::debian::translation-check translation="c4da598be91aaeba098bf112270ab920ef3f6977"

<h1>Debian installeren <current_release_jessie></h1>

<if-stable-release release="stretch">
<p><strong>Debian 8 werd vervangen door
<a href="../../stretch/">Debian 9 (<q>stretch</q>)</a>. Sommige van de
installatie-images hieronder zijn mogelijk niet langer beschikbaar of werken
niet meer. Het wordt aanbevolen om in de plaats daarvan stretch te
installeren.
</strong></p>
</if-stable-release>

<p>
<strong>Voor het installeren van Debian</strong> <current_release_jessie>
(<em>jessie</em>), kunt u een van de volgende images downloaden (alle i386 en amd64
CD/DVD-images zijn ook bruikbaar op USB-stick):
</p>

<div class="line">
<div class="item col50">
	<p><strong>netinst CD-image (meestal 150-280 MB)</strong></p>
		<netinst-images />
</div>


</div>

<div class="line">
<div class="item col50">
	<p><strong>volledige CD-sets</strong></p>
		<full-cd-images />
</div>

<div class="item col50 lastcol">
	<p><strong>volledige DVD-sets</strong></p>
		<full-dvd-images />
</div>

</div>

<div class="line">
<div class="item col50">
<p><strong>CD (via <a href="$(HOME)/CD/torrent-cd">BitTorrent</a>)</strong></p>
<full-cd-torrent />
</div>

<div class="item col50 lastcol">
<p><strong>DVD (via <a href="$(HOME)/CD/torrent-cd">BitTorrent</a>)</strong></p>
<full-dvd-torrent />
</div>

</div>

<div class="line">
<div class="item col50">
<p><strong>CD (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<p><strong>DVD (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-dvd-jigdo />
</div>


</div>

<div class="line">
<div class="item col50">
<p><strong>Blu-ray  (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-bluray-jigdo />
</div>

<div class="item col50 lastcol">
<p><strong>andere images (netboot, flexibele usb-stick, enz.)</strong></p>
<other-images />
</div>
</div>

<div id="firmware_nonfree" class="warning">
<p>
Indien een hardwareonderdeel van uw systeem <strong>het laden van niet-vrije firmware
vereist</strong> voor het stuurprogramma van een apparaat, kunt u een van de
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/jessie/current/">\
tar-archieven met gebruikelijke firmwarepakketten</a> gebruiken of een <strong>niet-officieel</strong> image downloaden met deze <strong>niet-vrije</strong> firmware. Instructies over het gebruik van deze tar-archieven en algemene informatie
over het laden van firmware tijdens de installatie is te vinden in de
Installatiehandleiding (zie onder Documentatie hierna).
</p>
<div class="line">
<div class="item col50">
<p><strong>netinst</strong> (gewoonlijk 240-290 MB) <strong>non-free</strong>
CD-images <strong>met firmware</strong></p>
<small-non-free-cd-images />
</div>
</div>
</div>



<p>
<strong>Opmerkingen</strong>
</p>
<ul>
    <li>
	Voor het downloaden van volledige CD- of DVD-images wordt het gebruik van
	BitTorrent of jigdo aanbevolen.
    </li><li>
	Voor de minder gebruikelijke architecturen is enkel een beperkt aantal
	images uit de CD- of DVD-set beschikbaar als ISO-bestand of via BitTorrent.
	De volledige sets zijn enkel via jigdo beschikbaar.
    </li><li>
	The multi-arch <em>CD</em> images support i386/amd64; the installation is similar to installing
	from a single architecture netinst image.
    </li><li>
	Het multi-arch <em>DVD</em>-image is bedoeld voor i386/amd64; de
	installatie is vergelijkbaar met een installatie met een
	volledig CD-image voor een enkele architectuur. De DVD bevat ook
	al de broncode voor de opgenomen pakketten.
    </li><li>
	Voor de netinst CD-images zijn verificatiebestanden (<tt>SHA256SUMS</tt>,
	<tt>SHA512SUMS</tt> en andere) te vinden in dezelfde map als de images.
    </li>
</ul>


<h1>Documentatie</h1>

<p>
<strong>Indien u slechts één document leest</strong> voor u met installeren
begint, lees dan onze
<a href="../i386/apa">Installatie-Howto</a> met een snel
overzicht van het installatieproces. Andere nuttige informatie is:
</p>

<ul>
<li><a href="../installmanual">Jessie Installatiehandleiding</a><br />
met uitgebreide installatie-instructies</li>
<li><a href="https://wiki.debian.org/DebianInstaller/FAQ">Debian-Installer FAQ</a>
en <a href="$(HOME)/CD/faq/">Debian-CD FAQ</a><br />
met algemene vragen en antwoorden</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Debian-Installer Wiki</a><br />
met door de gemeenschap onderhouden documentatie</li>
</ul>

<h1 id="errata">Errata</h1>

<p>
Dit is een lijst met gekende problemen in het installatiesysteem van
Debian <current_release_jessie>. Indien u bij het installeren van Debian op
een probleem gestoten bent en dit probleem hier niet vermeld vindt, stuur ons dan een
<a href="$(HOME)/releases/stable/i386/ch05s04.html#submit-bug">installatierapport</a>
waarin u de problemen beschrijft of
<a href="https://wiki.debian.org/DebianInstaller/BrokenThings">raadpleeg de wiki</a>
voor andere gekende problemen.
</p>

## Translators: copy/paste from devel/debian-installer/errata

<h3 id="errata-r11">Errata voor release 8.11</h3>
<dl class="gloss">

     <dt>pkgsel installeert geen updates met ABI-wijzigingen (stansdaard)</dt>

     <dd>Bug <a href="https://bugs.debian.org/908711">#908711</a>:
     tijdens een installatie met gebruik van netwerkpakketbronnen, ontbreken
     bij de geïnstalleerde beveiligingsupdates die updates welke een nieuw
     binair pakket vereisen door een ABI-wijziging van de kernel of een
     bibliotheek.

     <br /> <b>Status:</b> Dit werd gerepareerd in het installatiesysteem
     voor recentere releases (Debian 9 stretch). Voor installaties van Debian
     8 moeten beveiligingsupdates die nieuwe pakketten vereisen, handmatig
     geïnstalleerd worden, aangezien geen nieuw Debian installatiesysteem
     voorzien is:
     <br /> - Voer <code>apt-get upgrade --with-new-pkgs</code> uit
     <br /> - Herstart om de opwaardering af te ronden.
     </dd>

     <dt>APT was kwetsbaar voor een aanval van het type man-in-the-middle</dt>

     <dd>A bug in de HTTP transportmethode van APT
     (<a href="https://lists.debian.org/debian-lts-announce/2019/01/msg00014.html">CVE-2019-3462</a>)
     kon door een aanvaller die zich als een man-in-the-middle tussen APT
     en een spiegelserver bevond, uitgebuit worden om de installatie van
     bijkomende, kwaadaardige pakketten te veroorzaken.

     <br /> Dit kan beperkt worden door het gebruik van het netwerk bij de
     initiële installatie uit te schakelen en nadien een opwaardering uit te
     voeren volgens de instructies uit
     <a href="$(HOME)/lts/security/2019/dla-1637">DLA-1637</a>.

     <br /> <b>Status:</b> Dit werd gerepareerd in 8.11.1</dd>
</dl>

<h3 id="errata-r0">Errata voor release 8.0</h3>

<dl class="gloss">

     <dt>Desktop-installaties lukken niet met enkel CD#1</dt>

     <dd>Door de beperkte opslagruimte op de eerste CD, passen niet
     alle verwachte pakketten voor de GNOME desktop op CD#1. Gebruik voor een
     succesvolle installatie extra pakketbronnen (bijv. een tweede CD of een
     netwerkspiegelserver) of gebruik anders een DVD.

     <br /> <b>Status:</b> Het is onwaarschijnlijk dat er meer inspanningen
     kunnen worden gedaan om meer pakketten op CD#1 te krijgen. </dd>

     <dt>De opstartberichten op Powerpc zijn verouderd</dt>

     <dd>Bug <a href="https://bugs.debian.org/783569">#783569</a>:
     op powerpc-CD's is nog steeds sprake van het gebruik van de kernel
     commandoregel voor het selecteren van een desktop, maar dit is nu
     verouderd - gebruik in de plaats daarvan nu het tasksel menu.

     <br /> <b>Status:</b> Dit werd gerepareerd in 8.1</dd>

     <dt>Problemen bij het tegelijk installeren van meer dan één desktop-taak</dt>

     <dd>Bug <a href="https://bugs.debian.org/783571">#783571</a>:
     Het is niet mogelijk om tegelijkertijd GNOME en Xfce te installeren.
     Er bestaat een vereistenconflict dat ervoor verantwoordelijk is dat het
     installeren van de pakketten mislukt.

     <br /> <b>Status:</b> Dit werd gerepareerd in 8.1</dd>

</dl>

<if-stable-release release="jessie">
<p>
Voor de volgende release van Debian wordt gewerkt aan een verbeterde versie
van het installatiesysteem, die u ook kunt gebruiken om jessie te installeren.
Raadpleeg voor de concrete informatie
<a href="$(HOME)/devel/debian-installer/">de pagina van het Debian-Installer project</a>.
</p>
</if-stable-release>
