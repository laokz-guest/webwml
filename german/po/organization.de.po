# German translation of the Debian webwml modules
# Copyright (c) 2004 Software in the Public Interest, Inc.
# Dr. Tobias Quathamer <toddy@debian.org>, 2004, 2005, 2006, 2010, 2012, 2016, 2017, 2018, 2019.
# Helge Kreutzmann <debian@helgefjell.de>, 2007, 2009, 2011.
# Gerfried Fuchs <rhonda@debian.at>, 2002, 2003, 2004, 2008.
# Jens Seidel <tux-master@web.de>, 2004, 2006.
# Holger Wansing <linux@wansing-online.de>, 2011 - 2015.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2019-05-10 22:52+0100\n"
"Last-Translator: Dr. Tobias Quathamer <toddy@debian.org>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 2.0\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr "Delegations-Mail"

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr "Berufungs-Mail"

#. One male delegate
#: ../../english/intro/organization.data:18
msgid "<void id=\"male\"/>delegate"
msgstr "<void id=\"male\"/>delegiert"

#. One female delegate
#: ../../english/intro/organization.data:20
msgid "<void id=\"female\"/>delegate"
msgstr "<void id=\"female\"/>delegiert"

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:25
msgid "current"
msgstr "aktuell"

#: ../../english/intro/organization.data:27
#: ../../english/intro/organization.data:29
msgid "member"
msgstr "Mitglied"

#: ../../english/intro/organization.data:32
msgid "manager"
msgstr "Verwalter"

#: ../../english/intro/organization.data:34
msgid "SRM"
msgstr "SRM"

#: ../../english/intro/organization.data:34
msgid "Stable Release Manager"
msgstr "Release-Manager für Stable"

#: ../../english/intro/organization.data:36
msgid "wizard"
msgstr "Berater"

#. we only use the chair tag once, for techctte, I wonder why it's here.
#: ../../english/intro/organization.data:38
msgid "chair"
msgstr "Vorsitzender"

#: ../../english/intro/organization.data:41
msgid "assistant"
msgstr "Assistent"

#: ../../english/intro/organization.data:43
msgid "secretary"
msgstr "Schriftführer"

#: ../../english/intro/organization.data:59
#: ../../english/intro/organization.data:72
msgid "Officers"
msgstr "Direktoren"

#: ../../english/intro/organization.data:60
#: ../../english/intro/organization.data:96
msgid "Distribution"
msgstr "Distribution"

#: ../../english/intro/organization.data:61
#: ../../english/intro/organization.data:233
msgid "Communication and Outreach"
msgstr "Kommunikation und Outreach"

#: ../../english/intro/organization.data:63
#: ../../english/intro/organization.data:236
msgid "Data Protection team"
msgstr "Datenschutz-Team"

#: ../../english/intro/organization.data:64
#: ../../english/intro/organization.data:240
msgid "Publicity team"
msgstr "Öffentlichkeitsarbeits-Team"

#: ../../english/intro/organization.data:66
#: ../../english/intro/organization.data:307
msgid "Support and Infrastructure"
msgstr "Unterstützung und Infrastruktur"

#. formerly Custom Debian Distributions (CCDs); see https://blends.debian.org/blends/ch-about.en.html#s-Blends
#: ../../english/intro/organization.data:68
msgid "Debian Pure Blends"
msgstr "Debian Pure Blends (angepasste Debian-Distributionen)"

#: ../../english/intro/organization.data:75
msgid "Leader"
msgstr "Projektleiter"

#: ../../english/intro/organization.data:77
msgid "Technical Committee"
msgstr "Technischer Ausschuss"

#: ../../english/intro/organization.data:91
msgid "Secretary"
msgstr "Schriftführer"

#: ../../english/intro/organization.data:99
msgid "Development Projects"
msgstr "Entwicklungs-Projekte"

#: ../../english/intro/organization.data:100
msgid "FTP Archives"
msgstr "FTP-Archive"

#: ../../english/intro/organization.data:102
msgid "FTP Masters"
msgstr "FTP-Master"

#: ../../english/intro/organization.data:108
msgid "FTP Assistants"
msgstr "FTP-Mitarbeiter"

#: ../../english/intro/organization.data:113
msgid "FTP Wizards"
msgstr "FTP-Berater"

#: ../../english/intro/organization.data:117
msgid "Backports"
msgstr "Backports"

#: ../../english/intro/organization.data:119
msgid "Backports Team"
msgstr "Backports-Team"

#: ../../english/intro/organization.data:123
msgid "Individual Packages"
msgstr "Individuelle Pakete"

#: ../../english/intro/organization.data:124
msgid "Release Management"
msgstr "Release-Verwaltung"

#: ../../english/intro/organization.data:126
msgid "Release Team"
msgstr "Release-Team"

#: ../../english/intro/organization.data:139
msgid "Quality Assurance"
msgstr "Qualitätssicherung"

#: ../../english/intro/organization.data:140
msgid "Installation System Team"
msgstr "Installationssystem-Team"

#: ../../english/intro/organization.data:141
msgid "Release Notes"
msgstr "Veröffentlichungshinweise"

#: ../../english/intro/organization.data:143
msgid "CD Images"
msgstr "CD-Images"

#: ../../english/intro/organization.data:145
msgid "Production"
msgstr "Produktion"

#: ../../english/intro/organization.data:153
msgid "Testing"
msgstr "Testing"

#: ../../english/intro/organization.data:155
msgid "Cloud Team"
msgstr "Cloud-Team"

#: ../../english/intro/organization.data:159
msgid "Autobuilding infrastructure"
msgstr "Autobuilding-Infrastruktur"

#: ../../english/intro/organization.data:161
msgid "Wanna-build team"
msgstr "Wanna-build-Team"

#: ../../english/intro/organization.data:168
msgid "Buildd administration"
msgstr "Buildd-Administration"

#: ../../english/intro/organization.data:186
msgid "Documentation"
msgstr "Dokumentation"

#: ../../english/intro/organization.data:191
msgid "Work-Needing and Prospective Packages list"
msgstr "Liste der Arbeit-bedürfenden und voraussichtlichen Pakete"

#: ../../english/intro/organization.data:193
msgid "Debian Live Team"
msgstr "Debian-Live-Team"

#: ../../english/intro/organization.data:194
msgid "Ports"
msgstr "Portierungen"

#: ../../english/intro/organization.data:224
msgid "Special Configurations"
msgstr "Spezielle Konfigurationen"

#: ../../english/intro/organization.data:226
msgid "Laptops"
msgstr "Laptops"

#: ../../english/intro/organization.data:227
msgid "Firewalls"
msgstr "Firewalls"

#: ../../english/intro/organization.data:228
msgid "Embedded systems"
msgstr "Eingebettete Systeme"

#: ../../english/intro/organization.data:243
msgid "Press Contact"
msgstr "Pressekontakt"

#: ../../english/intro/organization.data:245
msgid "Web Pages"
msgstr "Webseiten"

#: ../../english/intro/organization.data:255
msgid "Planet Debian"
msgstr "Planet Debian"

#: ../../english/intro/organization.data:260
msgid "Outreach"
msgstr "Outreach"

#: ../../english/intro/organization.data:265
msgid "Debian Women Project"
msgstr "Debian-Women-Projekt"

#: ../../english/intro/organization.data:273
msgid "Anti-harassment"
msgstr "Gegen Mobbing/Belästigung"

#: ../../english/intro/organization.data:278
msgid "Events"
msgstr "Veranstaltungen"

#: ../../english/intro/organization.data:285
msgid "DebConf Committee"
msgstr "DebConf-Ausschuss"

#: ../../english/intro/organization.data:292
msgid "Partner Program"
msgstr "Partner-Programm"

#: ../../english/intro/organization.data:297
msgid "Hardware Donations Coordination"
msgstr "Hardware-Spenden-Koordination"

#: ../../english/intro/organization.data:310
msgid "User support"
msgstr "Benutzer-Unterstützung"

#: ../../english/intro/organization.data:377
msgid "Bug Tracking System"
msgstr "Fehlerdatenbank"

#: ../../english/intro/organization.data:382
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr "Mailinglisten-Verwaltung und -Archive"

#: ../../english/intro/organization.data:390
msgid "New Members Front Desk"
msgstr "Empfang (Front desk) für neue Mitglieder"

#: ../../english/intro/organization.data:396
msgid "Debian Account Managers"
msgstr "Debian-Konten-Verwalter"

#: ../../english/intro/organization.data:400
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""
"Um eine private Nachricht an alle DAMs zu schicken, verwenden Sie den GPG-"
"Schlüssel 57731224A9762EA155AB2A530CA8D15BB24D96F2."

#: ../../english/intro/organization.data:401
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "Schlüsselring-Verwalter (PGP und GPG)"

#: ../../english/intro/organization.data:405
msgid "Security Team"
msgstr "Sicherheitsteam"

#: ../../english/intro/organization.data:417
msgid "Consultants Page"
msgstr "Beraterseite"

#: ../../english/intro/organization.data:422
msgid "CD Vendors Page"
msgstr "CD-Distributoren-Seite"

#: ../../english/intro/organization.data:425
msgid "Policy"
msgstr "Policy"

#: ../../english/intro/organization.data:428
msgid "System Administration"
msgstr "System-Administration"

#: ../../english/intro/organization.data:429
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"Diese Adresse soll benutzt werden, wenn Probleme mit einer von Debians "
"Maschinen aufgetreten sind, auch wenn es sich um Passwort-Probleme handelt "
"oder wenn neue Pakete installiert werden sollen."

#: ../../english/intro/organization.data:438
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""
"Falls Sie Hardware-Probleme mit Debian-Rechnern bemerken, prüfen Sie bitte "
"die Übersicht der <a href=\"https://db.debian.org/machines.cgi\">Debian-"
"Rechner</a>, sie sollte Administrator-Informationen für jede Maschine "
"enthalten."

#: ../../english/intro/organization.data:439
msgid "LDAP Developer Directory Administrator"
msgstr "LDAP-Entwickler-Verzeichnis-Verwalter"

#: ../../english/intro/organization.data:440
msgid "Mirrors"
msgstr "Spiegel"

#: ../../english/intro/organization.data:447
msgid "DNS Maintainer"
msgstr "DNS-Verwalter"

#: ../../english/intro/organization.data:448
msgid "Package Tracking System"
msgstr "Paketverfolgungs-System"

#: ../../english/intro/organization.data:450
msgid "Treasurer"
msgstr "Schatzmeister"

#: ../../english/intro/organization.data:456
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""
"Anfragen zur Verwendung der <a name=\"trademark\" href=\"m4_HOME/trademark"
"\">Handelsmarken</a>"

#: ../../english/intro/organization.data:459
msgid "Salsa administrators"
msgstr "Salsa-Administratoren"

#: ../../english/intro/organization.data:470
msgid "Debian for children from 1 to 99"
msgstr "Debian für Kinder von 1 bis 99"

#: ../../english/intro/organization.data:473
msgid "Debian for medical practice and research"
msgstr "Debian für die medizinische Praxis und Forschung"

#: ../../english/intro/organization.data:476
msgid "Debian for education"
msgstr "Debian für Bildung"

#: ../../english/intro/organization.data:481
msgid "Debian in legal offices"
msgstr "Debian in Rechtsanwaltskanzleien"

#: ../../english/intro/organization.data:485
msgid "Debian for people with disabilities"
msgstr "Debian für Behinderte"

#: ../../english/intro/organization.data:489
msgid "Debian for science and related research"
msgstr "Debian für die Wissenschaft und zugehörige Forschung"

#: ../../english/intro/organization.data:492
msgid "Debian for astronomy"
msgstr "Debian für Astronomie"

#~ msgid "Alioth administrators"
#~ msgstr "Alioth-Administratoren"

#~ msgid "Publicity"
#~ msgstr "Öffentlichkeitsarbeit"

#~ msgid "Bits from Debian"
#~ msgstr "Neuigkeiten von Debian"

#~ msgid "Debian Maintainer (DM) Keyring Maintainers"
#~ msgstr "Schlüsselring-Betreuer für Debian-Betreuer (DM)"

#~ msgid "DebConf chairs"
#~ msgstr "DebConf-Leitung"

#~ msgid "Alpha (Not active: was not released with squeeze)"
#~ msgstr "Alpha (nicht aktiv: wurde nicht mit Squeeze veröffentlicht)"

#~ msgid "Vendors"
#~ msgstr "Distributoren"

#~ msgid "Handhelds"
#~ msgstr "Handheldcomputer"

#~ msgid "Marketing Team"
#~ msgstr "Marketing-Team"

#~ msgid "Key Signing Coordination"
#~ msgstr "Schlüssel-Signierungs-Koordination"

#~ msgid "Volatile Team"
#~ msgstr "Volatile-Team"

#~ msgid "Security Audit Project"
#~ msgstr "Sicherheits-Audit-Projekt"

#~ msgid "Testing Security Team"
#~ msgstr "Testing-Sicherheitsteam"

#~ msgid "current Debian Project Leader"
#~ msgstr "derzeitiger Debian-Projektleiter"

#~ msgid "Live System Team"
#~ msgstr "Live-System-Team"

#~ msgid "Auditor"
#~ msgstr "Kassenprüfer"
