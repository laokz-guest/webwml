#use wml::debian::template title="Errata pour l'installateur Debian"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="90ef9f0279b0248a336ff2fbfab930ec41f09ec4" maintainer="Baptiste Jammet" mindelta="1" maxdelta="1"
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"

# Translators:
# Nicolas Bertolissio, 2003-2008
# Thomas Peteul, 2008, 2009
# Simon Paillard, 2010
# David Prévot, 2010-2014
# Jean-Pierre Giraud, 2013
# Baptiste Jammet, 2014-2019

<h1><i>Errata</i> de la version <humanversion /></h1>

<p>
Voici une liste des problèmes connus dans la version <humanversion />
de l'installateur Debian.
Si le problème rencontré n'y est pas recensé, veuillez envoyer un <a
href="$(HOME)/releases/stable/amd64/ch05s04.html#submit-bug">compte-rendu d'installation</a>
(en anglais) décrivant le problème.
</p>


<dl class="gloss">

<dt>GNOME pourrait échouer à démarrer dans certaines configurations de machine virtuelle</dt>
<dd>Durant les tests de l'image Stretch Alpha 4, il a été remarqué que
GNOME échouait à démarrer en fonction de la configuration utilisée
pour les machines virtuelles. Il semble que l'utilisation de cirrus comme
émulateur de carte vidéo fonctionne.
<br />
<b>État :</b> en cours d'investigation.</dd>

<dt>L'installation d'un environnement de bureau pourrait échouer avec le seul premier CD</dt>

<dd>
En raison de sa taille,
la totalité des paquets du bureau GNOME ne rentre pas sur le premier CD.
Pour réussir cette installation, utilisez une source supplémentaire de paquets
(un second CD ou un miroir réseau) ou un DVD.
<br />
<b>État :</b> rien de plus ne peut être fait pour faire tenir
davantage de paquets sur le premier CD.
</dd>

<dt>Thème de l'installateur</dt>

<dd>Il n'y a pas encore de thème graphique pour Buster et l'installateur
utilise toujours celui de Stretch.
<br />
<b>État :</b>
corrigé dans la version Buster RC 1 :
<a href="https://bits.debian.org/2019/01/futurePrototype-will-be-the-default-theme-for-debian-10.html">futurePrototype</a>
a été intégré.</dd>

<dt>Raccourcis problématiques dans l'éditeur nano</dt>
<dd>La version de nano utilisée dans l'installateur n'utilise pas les
mêmes raccourcis que la version installée habituellement par le paquet.
En particulier, l'utilisation de <tt>C-s</tt> pour <q>Enregistrer</q>
semble être un problème pour ceux qui utilisent l'installateur à
travers une console série
(<a href="https://bugs.debian.org/915017">n° 915017</a>).
<br />
<b>État :</b> corrigé dans la version Buster RC 1.</dd>

<dt>Rendu graphique incorrect pour le Gujarati</dt>
<dd>Le rendu graphique pour la langue Gujarati est cassé à cause d'un
problème dans les fontes nécessaires pour ce langage.
Malheureusement, cela a été découvert trop tard pour la publication
de D-I Buster Alpha 4.
(<a href="https://bugs.debian.org/911705">n° 911705</a>).
<br />
<b>État :</b> en cours d'investigation, devrait être corrigé pour la
prochaine publication.</dd>

<dt>TLS ne fonctionne pas dans wget</dt>
<dd>Une modification dans la bibliothèque OpenSSL a provoqué des
problèmes pour les utilisateurs. Un fichier de configuration anciennement
optionnel est devenu obligatoire. Par conséquence, <tt>wget</tt> ne
prend pas en charge HTTPS dans l'installateur
(<a href="https://bugs.debian.org/926315">n° 926315</a>).
<br />
<b>État :</b>
la cause principale a été identifiée, et un correctif est attendu pour la version Buster RC 2.</dd>

<dt>Problème liés à l'entropie</dt>
<dd>Même avec HTTPS fonctionnel dans wget, il pourrait y avoir
des problèmes liés à l'entropie, en fonction de la disponibilité d'un
générateur de nombres aléatoires, ou un nombre suffisant d'évènements
pour alimenter une suite de bits aléatoires
(<a href="https://bugs.debian.org/923675">n° 923675</a>).
Habituellement, cela se traduit par un délai anormalement long lors
de la première connexion HTTPS, ou lors de la génération de la paire
de clefs SSH.
<br />
<b>État :</b> en cours d'investigation.</dd>

<dt>LUKS2 n'est pas compatible avec cryptodisk de GRUB</dt>
<dd>Il a été découvert tardivement que GRUB ne prenait pas en charge LUKS2.
Cela veut dire qu'il n'est pas possible d'utiliser <tt>GRUB_ENABLE_CRYPTODISK</tt>
sans une partition <tt>/boot</tt> séparée et non chiffrée.
(<a href="https://bugs.debian.org/927165">n° 927165</a>).
L'installateur ne prend pas en charge cette configuration,
mais il serait intéressant de mieux documenter cette limitation,
et d'avoir au moins la possibilité de choisir LUKS1 pendant l'installation.
<br />
<b>État :</b> quelques idées ont été exprimées dans le rapport de bogue.</dd>

<!-- Les choses devraient être mieux à partir de Jessie Beta 2...
<dt>Prise en charge de GNU/kFreeBSD</dt>

<dd>
Les images d’installation de GNU/kFreeBSD
sont victimes de plusieurs bogues importants
(<a href="https://bugs.debian.org/757985">nº 757985</a>,
<a href="https://bugs.debian.org/757986">nº 757986</a>,
<a href="https://bugs.debian.org/757987">nº 757987</a> et
<a href="https://bugs.debian.org/757988">nº 757988</a>).
Les porteurs ne refuseraient sans doute pas un peu
d’aide pour remettre l’installateur en service.
</dd>
-->
<!--
<dt>Accessibilité sur le système installé</dt>
<dd>Même si les technologies d'accessibilité sont utilisées pendant l'installation,
elles pourraient ne pas être activées automatiquement dans le système installé.
</dd>
-->

<!-- conservé pour une possible utilisation ultérieure...
	<dt>Installations de bureau non fonctionnelles sur i386 à partir du premier CD seul</dt>
	<dd>
	À cause de contraintes de place, tous les paquets nécessaires
	au bureau GNOME ne peuvent pas tenir sur le premier CD.

	Pour réussir l'installation, utilisez des sources de
	paquets supplémentaires (par exemple un deuxième CD ou
	un miroir sur le réseau) ou utilisez plutôt un DVD.
	<br />
	<b>État :</b> aucune ressource ne pourra probablement être consacrée
	à l'inclusion de paquets supplémentaires sur le premier CD.
	</dd>
-->

<!-- idem
	<dt>Problèmes possibles avec l'amorçage UEFI sur amd64</dt>
	<dd>
	Des problèmes ont été signalés lors de l'amorçage de
	l'installateur Debian en mode UEFI sur des systèmes amd64.

	Certains systèmes ne démarrent apparemment pas de façon fiable avec
	<code>grub-efi</code> et d'autres montrent des problèmes de
	corruption graphique de l'affichage d'écran initial d'installation.
	<br />
	Si vous rencontrez un de ces deux problèmes, veuillez soumettre un
	rapport de bogue aussi précis que possible, à la fois sur les symptômes
	et sur le matériel — cela devrait aider l'équipe à corriger ces problèmes.

	En attendant, il est possible de contourner le problème en désactivant l'UEFI et en
	poursuivant l'installation avec l'<q>ancien BIOS</q> ou en <q>mode de secours</q>.
	<br />
	<b>État :</b> d'autres corrections de bogues pourraient
	apparaître dans les diverses mises à jour de Wheezy.
	</dd>
-->

<!-- conservé pour une possible utilisation ultérieure...
	<dt>Architecture i386&nbsp;: plus de 32&nbsp;Mo requis pour
	l'installation</dt>
	<dd>
	La quantité minimale de mémoire requise pour une installation sur
	une architecture i386 est de 48&nbsp;Mo, au lieu de 32&nbsp;Mo précédemment.
	Nous espérons un retour au besoin initial de 32&nbsp;Mo dans une version
	ultérieure. Le besoin de mémoire peut aussi avoir changé pour d'autres
	architectures&nbsp;;
	</dd>
-->
</dl>
