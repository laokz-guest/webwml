#use wml::debian::translation-check translation="fb36d8d30ef43fdd50a0075131d4387545f6daaa" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes ont été découverts dans le serveur de base de
données MySQL. Les vulnérabilités sont corrigées en mettant MySQL à niveau
vers la nouvelle version amont 5.5.59, qui comprend d'autres changements.
Veuillez consulter les notes de publication de MySQL 5.5 et les annonces de
mises à jour critiques d'Oracle pour de plus amples détails :</p>

<ul>
<li><url "https://dev.mysql.com/doc/relnotes/mysql/5.5/en/news-5-5-59.html"></li>
<li><a href="http://www.oracle.com/technetwork/security-advisory/cpujan2018-3236628.html">\
http://www.oracle.com/technetwork/security-advisory/cpujan2018-3236628.html</a></li>
</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 5.5.59-0+deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets mysql-5.5.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1250.data"
# $Id: $
