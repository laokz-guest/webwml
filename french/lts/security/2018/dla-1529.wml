#use wml::debian::translation-check translation="ab3be4ee01879fd4484c795bbaa824377c218575" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pourraient conduire à une augmentation de droits, un déni de service ou une
fuite d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-3620">CVE-2018-3620</a>

<p>Plusieurs chercheurs ont découvert une vulnérabilité dans la manière dont la
conception des processeurs d’Intel a implémentée l’exécution spéculative des
instructions en combinaison avec le traitement des erreurs de page. Ce défaut
pourrait permettre à un attaquant contrôlant des processus sans droits de lire
la mémoire à partir d’adresses arbitraires (non contrôlées par l’utilisateur),
même à partir du noyau et de tous les processus en cours sur le système ou
aux croisement de limites invité/hôte pour lire la mémoire de l’hôte.</p>

<p>Ce problème ne concerne que les attaquants exécutant des processus normaux.
Un problème semblable
(<a href="https://security-tracker.debian.org/tracker/CVE-2018-3646">CVE-2018-3646</a>)
existe avec les invités et n’est pas encore corrigé.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-3639">CVE-2018-3639</a>

<p>Plusieurs chercheurs ont découvert que SSB (Speculative Store Bypass), une
fonction implémentée dans beaucoup de processeurs, pourrait être utilisée pour
lire des informations sensibles d’un autre contexte. En particulier, du code dans
un bac à sable logiciel pourrait être capable de lire des informations sensibles
hors du bac à sable. Ce problème est aussi connu comme Spectre variante 4.</p>

<p>Cette mise à jour permet de mitiger ce problème sur quelques processeurs x86
en désactivant SSB. Cela nécessite une mise à jour du micrologiciel qui n’est
pas libre. DLA 1446-1 et DLA 1506-1 fournissaient cela pour quelques processeurs
d’Intel. Pour les autres processeurs, cela pourra être inclus dans une mise à
jour du système BIOS ou du micrologiciel UEFI, ou dans une future mise à jour
des paquets intel-microcode ou amd64-microcode.</p>

<p>Désactiver SSB peut réduire les performances de manière significative, aussi,
par défaut, cela est réalisé que dans les tâches qui utilisent la fonction
seccomp. Les applications qui veulent cette mitigation doivent la demander
explicitement à travers l’appel système prctl(). Les utilisateurs peuvent
contrôler où cette mitigation est activée avec le paramètre
spec_store_bypass_disable du noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5391">CVE-2018-5391</a>

<p>(FragmentSmack)</p>

<p>Juha-Matti Tilli a découvert un défaut dans la manière dont le noyau Linux
gère le réassemblage de paquets fragmentés IPv4 et IPv6. Un attaquant distant
peut exploiter ce défaut pour déclencher des algorithmes de réassemblage de
fragments, coûteux en temps et calcul, en envoyant des paquets contrefaits pour
l'occasion, aboutissant à un déni de service.</p>

<p>Ce défaut est atténué en réduisant les limites par défaut d'utilisation
de la mémoire pour des paquets fragmentés incomplets. La même atténuation
peut être obtenue sans nécessiter de redémarrage, en réglant sysctls :</p>

<p>net.ipv4.ipfrag_low_thresh = 196608<br>
net.ipv6.ip6frag_low_thresh = 196608<br>
net.ipv4.ipfrag_high_thresh = 262144<br>
net.ipv6.ip6frag_high_thresh = 262144</p>

<p>Les valeurs par défaut peuvent encore être augmentées par une
configuration locale si nécessaire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-6554">CVE-2018-6554</a>

<p>Une fuite de mémoire a été découverte dans la fonction irda_bind du
sous-système irda. Un utilisateur local peut tirer avantage de ce défaut
pour provoquer un déni de service (consommation de mémoire).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-6555">CVE-2018-6555</a>

<p>Un défaut a été découvert dans la fonction irda_setsockopt du
sous-système irda, permettant à un utilisateur local de provoquer un déni
de service (utilisation de mémoire après libération et plantage du
système).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7755">CVE-2018-7755</a>

<p>Brian Belleville a découvert un défaut dans la fonction fd_locked_ioctl
du pilote du lecteur de disquette dans le noyau Linux. Le pilote du lecteur
de disquette copie un pointeur du noyau dans la mémoire de l'utilisateur en
réponse à l'ioctl FDGETPRM. Un utilisateur local doté d'un accès à un
lecteur de disquette peut tirer avantage de ce défaut pour découvrir
l'emplacement du code et des données du noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-9363">CVE-2018-9363</a>

<p>L'implémentation HIDP de Bluetooth ne vérifiait pas correctement la
longueur des messages de rapport reçus. Un périphérique HIDP appairé
pourrait utiliser cela pour provoquer un dépassement de tampon, menant à un
déni de service (corruption de mémoire ou plantage) ou éventuellement à
l'exécution de code distant.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-9516">CVE-2018-9516</a>
<p>L'interface des événements HID dans debugfs ne limitait pas correctement
la longueur des copies dans les tampons de l'utilisateur. Un utilisateur
local doté de l'accès à ces fichiers pourrait utiliser cela pour provoquer
un déni de service (corruption de mémoire ou plantage) ou éventuellement
une augmentation de droits. Néanmoins, par défaut, debugfs est seulement
accessible à l'utilisateur root.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10021">CVE-2018-10021</a>

<p>Un attaquant physiquement présent débranchant un câble SAS peut causer un
déni de service (fuite de mémoire et WARN).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10323">CVE-2018-10323</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2018-13093">CVE-2018-13093</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2018-13094">CVE-2018-13094</a>

<p>Wen Xu du SSLab de Gatech a signalé plusieurs défauts potentiels de
déréférencement de pointeur NULL pouvant être déclenchés lors du montage et de
l’utilisation d’un volume XFS. Un attaquant capable de monter des volumes XFS
contrefaits pourrait utiliser cela pour provoquer un déni de service (plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10876">CVE-2018-10876</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2018-10877">CVE-2018-10877</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2018-10878">CVE-2018-10878</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2018-10879">CVE-2018-10879</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2018-10880">CVE-2018-10880</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2018-10881">CVE-2018-10881</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2018-10882">CVE-2018-10882</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2018-10883">CVE-2018-10883</a>

<p>Wen Xu de SSLab à Gatech a signalé que des volumes ext4 contrefaits pourraient
déclencher un plantage ou une corruption de mémoire. Un attaquant capable de
monter des volumes ext4 arbitraires pourrait utiliser cela pour un déni de
service ou éventuellement pour une augmentation de droits.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10902">CVE-2018-10902</a>

<p>Le pilote rawmidi du noyau ne protège pas des accès concurrents qui
mènent à un défaut de double-realloc (double libération de zone de
mémoire). Un attaquant local peut tirer avantage de ce problème pour une
augmentation de droits.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-13405">CVE-2018-13405</a>

<p>Jann Horn a découvert que la fonction inode_init_owner dans fs/inode.c dans
le noyau Linux permet à des utilisateurs locaux de créer des fichiers avec un
groupe propriétaire inattendu, permettant à des attaquants d’augmenter leurs
droits en rendant un fichier simple exécutable et SGID.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-13406">CVE-2018-13406</a>

<p>Dr Silvio Cesare de InfoSect a signalé un dépassement potentiel d'entier dans
le pilote uvesafb. Un utilisateur local avec permission d’accéder à un tel
périphérique pourrait être capable d’utiliser cela pour un déni de service ou
une augmentation de droits.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14609">CVE-2018-14609</a>

<p>Wen Xu du SSLab de Gatech a signalé un potentiel déréférencement de
pointeur NULL dans l'implémentation de F2FS. Un attaquant capable de
monter un volume F2FS contrefait pourrait utiliser cela pour provoquer un
déni de service (plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14617">CVE-2018-14617</a>

<p>Wen Xu du SSLab de Gatech a signalé un potentiel déréférencement de
pointeur NULL dans implémentation de HFS+. Un attaquant capable de
monter un volume HFS+ contrefait pourrait utiliser cela pour provoquer un
déni de service (plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14633">CVE-2018-14633</a>

<p>Vincent Pelletier a découvert un défaut de dépassement de pile dans la
fonction chap_server_compute_md5() du code de la cible iSCSI. Un attaquant
distant non authentifié peut tirer avantage de ce défaut pour provoquer un
déni de service ou éventuellement pour obtenir un accès non autorisé à des
données exportées par une cible iSCSI.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14634">CVE-2018-14634</a>

<p>Qualys a signalé un dépassement d'entier dans l’initialisation de la pile
pour les exécutables ELF, qui peut forcer la pile à chevaucher les chaînes
d’argument ou d’environnement. Un utilisateur local peut utiliser cela pour
contrecarrer le filtrage de variable d’environnement dans les programmes setuid,
aboutissant à une augmentation de droits.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14678">CVE-2018-14678</a>

<p>M. Vefa Bicakci et Andy Lutomirski ont découvert un défaut dans le code
de sortie du noyau sur les systèmes amd64 exécutés comme clients de
paravirtualisation (PV) Xen. Un utilisateur local pourrait utiliser cela
pour provoquer un déni de service (plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14734">CVE-2018-14734</a>

<p>Un bogue d'utilisation de mémoire après libération a été découvert dans
le gestionnaire de communication InfiniBand. Un utilisateur local pourrait
utiliser cela pour provoquer un déni de service (plantage ou corruption de
mémoire) ou une possible augmentation de droits.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-15572">CVE-2018-15572</a>

<p>Esmaiel Mohammadian Koruyeh, Khaled Khasawneh, Chengyu Song et Nael
Abu-Ghazaleh de l'université de Californie, Riverside, ont signalé une
variante de Spectre variante 2, nommée SpectreRSB. Un utilisateur local
peut être capable d'utiliser cela pour lire des informations sensibles
à partir de processus appartenant à d'autres utilisateurs.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-15594">CVE-2018-15594</a>

<p>Nadav Amit a signalé que certains appels indirects de fonctions utilisés
dans les clients paravirtualisés étaient vulnérables à Spectre variante 2.
Un utilisateur local peut être capable d'utiliser cela pour lire des
informations sensibles à partir du noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16276">CVE-2018-16276</a>

<p>Jann Horn a découvert que le pilote yurex ne limitait pas correctement
la longueur des copies dans les tampons de l'utilisateur. Un utilisateur
local doté d'un accès à un nœud de périphérique yurex pourrait utiliser
cela pour provoquer un déni de service (corruption de mémoire ou plantage)
ou éventuellement une augmentation de droits.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16658">CVE-2018-16658</a>

<p>Le pilote de cdrom ne valide pas correctement le paramètre de l'ioctl
CDROM_DRIVE_STATUS. Un utilisateur doté d'un accès à un périphérique de
cdrom pourrait utiliser cela pour lire des informations sensibles provenant
du noyau ou provoquer un déni de service (plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-17182">CVE-2018-17182</a>

<p>Jann Horn a découvert que la fonction vmacache_flush_all ne gère pas
correctement les débordements de numéro de séquence. Un utilisateur local
peut tirer avantage de ce défaut pour déclencher une utilisation de mémoire
après libération, provoquant un déni de service (plantage ou corruption de
mémoire) ou une augmentation de droits.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 3.16.59-1. Cette version inclut aussi un correctif pour le bogue
n° 898137 et plusieurs autres inclus dans les mises à jour amont.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux. Puisque les noms
 d’ABI du noyau et des paquets binaires ont changé, vous aurez besoin
d’utiliser une commande upgrade installant de nouvelles dépendances, telle que
<q>apt upgrade</q> ou « apt-get upgrade --with-new-pkgs ».</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1529.data"
# $Id: $
