#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Un utilisateur sans privilèges de dblink ou postgres_fdw pourrait contourner
les vérifications prévues pour empêcher l’utilisation des accréditations du côté
serveur, telles que le fichier ~/.pgpass possédé par l’utilisateur du système
d’exploitation exécutant le serveur. Les serveurs permettant l’authentification
de pair sur des connexions locales sont particulièrement vulnérables. D’autres
attaques telles qu’une injection SQL dans une session postgres_fdw sont aussi
possibles. Attaquer postgres_fdw de cette façon requiert la possibilité de créer
un objet étranger de serveur avec des paramètres de connexion choisis, mais
n’importe quel utilisateur avec accès à dblink pourrait exploiter ce problème.
En général, un attaquant avec la possibilité de choisir les paramètres de
connexion pour une application utilisant libpq pourrait causer des troubles,
toutefois des scénarios d’attaque plausibles sont difficiles à imaginer. Nos
mercis à Andrew Krasichkov pour le signalement de ce problème.</p>

<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans
la version 9.4.19-0+deb8u1.</p>
<p>Nous vous recommandons de mettre à jour vos paquets postgresql-9.4.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1464.data"
# $Id: $
