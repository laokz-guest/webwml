#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pourraient conduire à un déni de service.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1093">CVE-2018-1093</a>

<p>Wen Xu a signalé qu'une image contrefaite de système de fichiers ext4
pourrait déclencher une lecture hors limites dans la fonction
ext4_valid_block_bitmap(). Un utilisateur local capable de monter des
systèmes de fichiers arbitraires pourrait utiliser cela pour un déni de
service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1130">CVE-2018-1130</a>

<p>Le logiciel syzbot a trouvé que l’implémentation de DCCP de sendmsg() ne
vérifie pas l’état de socket, conduisant éventuellement à un déréférencement de
pointeur NULL. Un utilisateur local pourrait utiliser cela pour provoquer un
déni de service (plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-8897">CVE-2018-8897</a>

<p>Nick Peterson de Everdox Tech LLC a découvert que les exceptions #DB qui
étaient différées par MOV SS ou POP SS n'étaient pas correctement gérées,
permettant à un utilisateur non privilégié de planter le noyau et de
provoquer un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10940">CVE-2018-10940</a>

<p>Dan Carpenter a signalé que le pilote de disque optique (cdrom) ne validait
pas correctement le paramètre pour l’ioctl CDROM_MEDIA_CHANGED. Un utilisateur
ayant accès à un périphérique cdrom pourrait utiliser cela pour provoquer un
déni de service (plantage).</p></li>
</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 3.2.102-1. Cette version inclut aussi des corrections de bogue de la
version 3.2.102 amont, comprenant un correctif pour une régression dans
l’implémentation de SCTP pour la version 3.2.101.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1392.data"
# $Id: $
