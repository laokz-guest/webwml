<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Florian Larysch and Bram Moolenaar discovered that vim, an enhanced vi
editor, does not properly validate values for the the <q>filetype</q>,
<q>syntax</q> and <q>keymap</q> options, which may result in the execution of
arbitrary code if a file with a specially crafted modeline is opened.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2:7.3.547-7+deb7u1.</p>

<p>We recommend that you upgrade your vim packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-718.data"
# $Id: $
