<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Various security issues where found and fixed in phpmyadmin in wheezy.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4412">CVE-2016-4412</a> / PMASA-2016-57

    <p>A user can be tricked in following a link leading to phpMyAdmin, which
    after authentication redirects to another malicious site.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6626">CVE-2016-6626</a> / PMASA-2016-49

    <p>In the fix for PMASA-2016-57, we didn't have sufficient checking and was
    possible to bypass whitelist.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9849">CVE-2016-9849</a> / PMASA-2016-60

    <p>Username deny rules bypass (AllowRoot &amp; Others) by using Null Byte.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9850">CVE-2016-9850</a> / PMASA-2016-61

    <p>Username matching for the allow/deny rules
    may result in wrong matches and detection of the username in the rule due
    to non-constant execution time.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9861">CVE-2016-9861</a> / PMASA-2016-66

    <p>In the fix for PMASA-2016-49, we has buggy checks and was possible to
    bypass whitelist.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9864">CVE-2016-9864</a> / PMASA-2016-69

    <p>Multiple SQL injection vulnerabilities.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9865">CVE-2016-9865</a> / PMASA-2016-70

    <p>Due to a bug in serialized string parsing, it was possible to bypass the
    protection offered by PMA_safeUnserialize() function.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
4:3.4.11.1-2+deb7u7.</p>

<p>We recommend that you upgrade your phpmyadmin packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-757.data"
# $Id: $
