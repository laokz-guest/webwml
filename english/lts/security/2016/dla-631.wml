<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that there were two vulnerabilities in unadf, a tool to
extract files from an Amiga Disk File dump (.adf):</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-1243">CVE-2016-1243</a>

    <p>stack buffer overflow caused by blindly trusting on
    pathname lengths of archived files.</p>

    <p>Stack allocated buffer sysbuf was filled with sprintf() without any
    bounds checking in extracTree() function.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-1244">CVE-2016-1244</a>

    <p>execution of unsanitized input</p>

    <p>Shell command used for creating directory paths was constructed by
    concatenating names of archived files to the end of the command
    string.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, this issue has been fixed in unadf version
0.7.11a-3+deb7u1.</p>

<p>We recommend that you upgrade your unadf packages.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-631.data"
# $Id: $
