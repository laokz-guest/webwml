<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The Samba 2:3.6.6-6+deb7u9 release, issued by the DSA-3548-1, introduced
different regressions causing trust relationship with Win 7 domains to
fail. The fix for the <a href="https://security-tracker.debian.org/tracker/CVE-2016-2115">CVE-2016-2115</a> has been reverted, so administrators
should set 'client signing = required' instead.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2:3.6.6-6+deb7u10. More information is available in the NEWS file
included in this samba release.</p>

<p>We recommend that you upgrade your samba packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-509.data"
# $Id: $
