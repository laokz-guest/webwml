<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Gergely Gábor Nagy from Tresorit discovered that libcrypto++, a C++
cryptographic library, contained a bug in several ASN.1 parsing
routines. This would allow an attacker to remotely cause a denial of
service.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
5.6.1-6+deb7u3.</p>

<p>We recommend that you upgrade your libcrypto++ packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-766.data"
# $Id: $
