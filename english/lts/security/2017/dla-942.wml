<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7885">CVE-2017-7885</a>

      <p>Artifex jbig2dec 0.13 has a heap-based buffer over-read leading to
      denial of service (application crash) or disclosure of sensitive
      information from process memory, because of an integer overflow
      in the jbig2_decode_symbol_dict function in jbig2_symbol_dict.c
      in libjbig2dec.a during operation on a crafted .jb2 file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7975">CVE-2017-7975</a>

      <p>Artifex jbig2dec 0.13, as used in Ghostscript, allows out-of-bounds
      writes because of an integer overflow in the jbig2_build_huffman_table
      function in jbig2_huffman.c during operations on a crafted JBIG2 file,
      leading to a denial of service (application crash) or possibly
      execution of arbitrary code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7976">CVE-2017-7976</a>

      <p>Artifex jbig2dec 0.13 allows out-of-bounds writes and reads because
      of an integer overflow in the jbig2_image_compose function in
      jbig2_image.c during operations on a crafted .jb2 file, leading
      to a denial of service (application crash) or disclosure of
      sensitive information from process memory.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.13-4~deb7u2.</p>

<p>We recommend that you upgrade your jbig2dec packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-942.data"
# $Id: $
