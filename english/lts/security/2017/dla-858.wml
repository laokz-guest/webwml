<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that Wireshark, a network protocol analyzer,
contained several vulnerabilities in the dissectors for ASTERIX,
DHCPv6, LDSS, IAX2, WSP and RTMPT and the NetScaler and K12 file
parsers, that could lead to various crashes, denial-of-service, or
execution of arbitrary code.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.12.1+g01b65bf-4+deb8u6~deb7u7.</p>

<p>We recommend that you upgrade your wireshark packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-858.data"
# $Id: $
