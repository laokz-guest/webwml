<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Joern Schneeweisz discovered that git, a distributed revision control
system, did not correctly handle maliciously constructed ssh://
URLs. This allowed an attacker to run an arbitrary shell command, for
instance via git submodules.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1:1.7.10.4-1+wheezy5.</p>

<p>We recommend that you upgrade your git packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1068.data"
# $Id: $
