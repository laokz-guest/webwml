<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that there was a denial of service (DoS) vulnerability
in the nginx web/proxy server.</p>

<p>As there was no validation for the size of a 64-bit atom in an MP4 file,
this could have led to a CPU hog when the size was 0, or various other
problems due to integer underflow when the calculating atom data size,
including segmentation faults or even worker-process memory disclosure.</p>

<p>For Debian 8 <q>Jessie</q>, this issue has been fixed in nginx version
1.6.2-5+deb8u6.</p>

<p>We recommend that you upgrade your nginx packages.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1572.data"
# $Id: $
