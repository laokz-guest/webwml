<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A vulnerability in dropbear, a lightweight SSH2 server and client, making it
possible to guess valid usernames has been found:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-15599">CVE-2018-15599</a>

    <p>The recv_msg_userauth_request function in svr-auth.c in is prone
    to a user enumeration vulnerability, similar to 
    <a href="https://security-tracker.debian.org/tracker/CVE-2018-15473">CVE-2018-15473</a> in OpenSSH.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
2014.65-1+deb8u3.</p>

<p>We recommend that you upgrade your dropbear packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1476.data"
# $Id: $
