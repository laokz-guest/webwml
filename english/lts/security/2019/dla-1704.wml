<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Vulnerabilities have been discovered in nss, the Mozilla Network
Security Service library.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-12404">CVE-2018-12404</a>

    <p>Cache side-channel variant of the Bleichenbacher attack</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18508">CVE-2018-18508</a>

    <p>NULL pointer dereference in several CMS functions resulting in a
    denial of service</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2:3.26-1+debu8u4.</p>

<p>We recommend that you upgrade your nss packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1704.data"
# $Id: $
